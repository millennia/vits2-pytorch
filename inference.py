## LJSpeech
import torch

import io
import commons
import utils
import argparse
import subprocess
import os
import signal

from models import SynthesizerTrn
from text.symbols import symbols
from text import text_to_sequence

from scipy.io.wavfile import write


def signal_handler(sig, frame):
    inp = input("ctrl+c pressed, quit now? (y/n)")
    if inp == "y":
        exit(0)

signal.signal(signal.SIGINT, signal_handler)

def get_text(text, hps):
    text_norm = text_to_sequence(text, hps.data.text_cleaners)
    if hps.data.add_blank:
        text_norm = commons.intersperse(text_norm, 0)
    text_norm = torch.LongTensor(text_norm)
    return text_norm

parser = argparse.ArgumentParser()
parser.add_argument(
    "-c",
    "--config",
    type=str,
    default="./configs/base.json",
    help="JSON file for configuration",
)
parser.add_argument("-m", "--model", type=str, required=True, help="Model path")
parser.add_argument("-t", "--text", type=str, default="こんばんわ、今日はいい天気ですね。", help="Text to TTS")
parser.add_argument("-o", "--output", type=str, default="sample_vits2", help="output_file, .wav will be appended")
parser.add_argument("-ns", "--noisescale", type=float, default=0.667)
parser.add_argument("-nsw", "--noisescalew", type=float, default=0.8)
parser.add_argument("-i", "--interactive", action='store_true', help="Interactive mode")
args = parser.parse_args()

#CONFIG_PATH = "./configs/vits2_grisaia_yumiko.json"
CONFIG_PATH = args.config
MODEL_PATH = args.model
#MODEL_PATH = "./logs/grisaia_yumiko/G_11000.pth" # G_114000.pth
#TEXT = "え…、どこ行くの？"
TEXT = args.text

#OUTPUT_WAV_PATH = "sample_vits2.wav"
OUTPUT_WAV_PATH = args.output

hps = utils.get_hparams_from_file(CONFIG_PATH)

if (
    "use_mel_posterior_encoder" in hps.model.keys()
    and hps.model.use_mel_posterior_encoder == True
):
    print("Using mel posterior encoder for VITS2")
    posterior_channels = 80  # vits2
    hps.data.use_mel_posterior_encoder = True
else:
    print("Using lin posterior encoder for VITS1")
    posterior_channels = hps.data.filter_length // 2 + 1
    hps.data.use_mel_posterior_encoder = False

net_g = SynthesizerTrn(
    len(symbols),
    posterior_channels,
    hps.train.segment_size // hps.data.hop_length,
    **hps.model
).cpu()
_ = net_g.eval()

_ = utils.load_checkpoint(MODEL_PATH, net_g, None)

#avg = torch.optim.swa_utils.AveragedModel(net_g)
#torch.save("G_TEST", avg)
#exit(0)

# text, ids
def read_file(filepath):
    res = []
    with open(TEXT) as f:
       res = f.read().split("\n")
       for i in range(0, len(res)):
           if "「" in res[i]:
               loc = res[i].find("「")
               loc2 = res[i].find("」")
               if loc != -1 and loc != -1:
                   res[i] = res[i][loc+1:loc2]
           res[i] = (res[i], get_text(res[i], hps))
    return res

def play_mpv(audio_data, sampling_rate):
    b = io.BytesIO()
    write(data=audio, rate=sampling_rate, filename=b)
    subprocess.Popen(["mpv", "--no-terminal", "-"], stdin=subprocess.PIPE).communicate(input=b.read())

def infer(net_g, x_tst, x_st_length, noisescale, noisescalew, length_scale=1):
    return net_g.infer(x_tst, x_tst_lengths, noisescale, noisescalew, length_scale=length_scale)[0][0, 0].data.cpu().float().numpy()

if args.interactive:
    while 1:
        inp = input(">>")
        if inp == "q":
            break
        
        ids = get_text(inp, hps)
        x_tst = ids.cpu().unsqueeze(0)
        x_tst_lengths = torch.LongTensor([ids.size(0)]).cpu()
        audio = (
            net_g.infer(
                    x_tst, x_tst_lengths, args.noisescale, args.noisescalew, length_scale=1)[0][0, 0]
                .data.cpu()
                .float()
                .numpy()
        )
        play_mpv(audio, hps.data.sampling_rate)
    exit(0)

TEXT_LIST = []
stn_tst = ""
if os.path.isfile(TEXT):
    TEXT_LIST = read_file(TEXT)
else:
    TEXT_LIST += [(TEXT, get_text(TEXT, hps))]

audio_list = []
with torch.no_grad():
    for text, ids in TEXT_LIST:           
        x_tst = ids.cpu().unsqueeze(0)
        x_tst_lengths = torch.LongTensor([ids.size(0)]).cpu()
        if len(TEXT_LIST) > 50:
            audio = infer(net_g, x_tst, x_tst_lengths, args.noisescale, args.noisescalew)
            play_mpv(audio, hps.data.sampling_rate)
        else:
            audio_list += [(
                net_g.infer(
                    x_tst, x_tst_lengths, args.noisescale, args.noisescalew, length_scale=1)[0][0, 0]
                .data.cpu()
                .float()
                .numpy()
            )]

if len(audio_list) == 0:
    exit(0)

if OUTPUT_WAV_PATH == "-":
    cnt = 0
    for audio in audio_list:
        b = io.BytesIO()
        write(data=audio, rate=hps.data.sampling_rate, filename=b)
        print(TEXT_LIST[cnt][0])
        subprocess.Popen(["mpv", "--no-terminal", "-"], stdin=subprocess.PIPE).communicate(input=b.read())
        cnt += 1
else:
    cnt = 0
    for audio in audio_list:
        write(data=audio, rate=hps.data.sampling_rate, filename=OUTPUT_WAV_PATH + str(cnt) + ".wav")
        cnt += 1
        print("saved")

del net_g