# VITS2: Improving Quality and Efficiency of Single-Stage Text-to-Speech with Adversarial Learning and Architecture Design
### Jungil Kong, Jihoon Park, Beomjeong Kim, Jeongmin Kim, Dohee Kong, Sangjin Kim 

original repo here:<br>
https://github.com/p0p4k/vits2_pytorch<br>

Changed:<br>
* Added a simple train-script for single GPU. Also tried optimizing but i have no idea what i'm doing.<br>
* Changed dataloader to load in entire dataset in RAM rather than from disk since i work with smaller datasets.<br>
* Some misc changes (like finetune option and graceful shutdown when 1 thread dataloader)<br>

Few things more i wanted to test out but can't due to lack of VRAM. Will prolly come back to this when i get better hardware.<br>
Dataloader is also a bit bugged on pytorch so the training runs the risk of running out of open filehandles.<br>
https://stackoverflow.com/questions/48250053/pytorchs-dataloader-too-many-open-files-error-when-no-files-should-be-open<br>
Increasing amount of filehandles or setting torch.multiprocessing.set_sharing_strategy('file_system') might fix it.