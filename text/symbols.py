'''
Defines the set of symbols used in text input to the model.
'''

# ○ add "beep" ♪?
_pad        = '_'
_punctuation = ',.!?-~…'
_letters = 'AEINOQUabdefghijklmnoprstuvwyzʃʧʦɯɹəɥ⁼ʰ`→↓↑ '

#hirag = [chr(h) for h in range(0x3041, 0x3096)]
#kata = [chr(k) for k in range(0x30A0, 0x30ff)]
#kanji = [chr(k) for k in range(0x3400, 0x4db5)]
#kanji = kanji + [chr(k) for k in range(0x4e00, 0x9fcb)]
#kanji = kanji + [chr(k) for k in range(0x9fcb, 0xfa6a)]
#fullw = [chr(fw) for fw in range(0xff01, 0xff5e)]

# Export all symbols:
symbols = [_pad] + list(_punctuation) + list(_letters)
#symbols = [_pad] + list(_punctuation) + list(_letters) + hirag + kata + kanji
# Special symbol ids
SPACE_ID = symbols.index(" ")
